FROM golang:1.22.0

COPY ./app /app/

WORKDIR /app

RUN go build -o ./bin/server .

EXPOSE 8080

ENTRYPOINT [ "./bin/server" ]
